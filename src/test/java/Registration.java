import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.not;

public class Registration {
    public static final String site_url = "https://www.cheapdomains.com.au/register/member_register.php";

    @BeforeClass
    public void setUp(){
        Configuration.browser = "firefox";
    }

    @Test(dataProvider = "provideData", dataProviderClass = TestParameterDataProvider.class)
    public void testRegistration(User user, Address address, Login login){
        RegistrationPage registrationPage = open(site_url, RegistrationPage.class);
        registrationPage.setUser(user);
        registrationPage.setAddress(address);
        registrationPage.setLogin(login);
        registrationPage.submitClick();
        assertThat(url(), not(containsString("register")));
    }

    @AfterMethod
    public void close(){
        closeWebDriver();
    }

}
