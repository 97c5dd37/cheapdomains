import org.testng.annotations.DataProvider;

public class TestParameterDataProvider {
    @DataProvider(name = "provideData")
    public Object[][] provideData() {

        return new Object[][] {
                { new User("Martin", "Fauler", "test1@test.com"), new Address("Some add", "Tokio", "023522", "Angola",
                        "DE", "+1234566", "abn"), new Login("user1", "pass") },
                { new User("Martin", "Fauler", "test1@test.com"), new Address("Some add", "Tokio", "023522", "Angola",
                        "DE", "+1234566", "abn"), new Login("", "") }
        };
    }
}
