import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage {

	@FindBy(name="first_name")
	private SelenideElement firstName;
	@FindBy(name="last_name")
	private SelenideElement lastName;
	@FindBy(name="address")
	private SelenideElement address;
	@FindBy(name="city")
	private SelenideElement city;
    @FindBy(name="post_code")
    private SelenideElement postCode;
    @FindBy(name="country")
    private SelenideElement country;
    @FindBy(name="state")
    private SelenideElement state;
    @FindBy(name="phone")
    private SelenideElement phone;
    @FindBy(name="email")
    private SelenideElement email;
    @FindBy(name="abn")
    private SelenideElement abn;
    @FindBy(name="username")
    private SelenideElement username;
    @FindBy(name="password")
    private SelenideElement password;
	@FindBy(xpath="//*[@id=\"content_pad\"]/form[2]/table[3]/tbody/tr/td[2]/input")
	private SelenideElement submit;



    public void setUser(User user){
        firstName.setValue(user.getFirstName());
        lastName.setValue(user.getLastName());
        email.setValue(user.getEmail());
    }
    public void setAddress(Address val){
        address.setValue(val.getAddress());
        city.setValue(val.getCity());
        postCode.setValue(val.getPostCode());
        country.selectOptionContainingText(val.getCountry());
        state.setValue(val.getState());
        phone.setValue(val.getPhone());
        abn.setValue(val.getAbn());
    }

    public void setLogin(Login login){
        username.setValue(login.getUsername());
        password.setValue(login.getPassword());
    }

    public void submitClick(){
        submit.click();
    }

}
