import lombok.Data;
@Data
public class Login {
    private String username;
    private String password;

    public Login(String _username, String _password){
        this.username = _username;
        this.password = _password;
    }
}
