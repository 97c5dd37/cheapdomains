import com.codeborne.selenide.SelenideElement;
import lombok.Data;

@Data
public class Address {
    private String address;
    private String city;
    private String postCode;
    private String country;
    private String state;
    private String phone;
    private String abn;
    public Address(String _address, String _city, String _postCode, String _country, String _state, String _phone, String _abn){
        this.address = _address;
        this.city = _city;
        this.postCode = _postCode;
        this.country = _country;
        this.state = _state;
        this.phone = _phone;
        this.abn = _abn;

    }
}
