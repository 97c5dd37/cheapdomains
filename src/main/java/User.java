import lombok.Data;

@Data
public class User {
    private String firstName;
    private String lastName;
    private String email;
    public User(String first, String last, String _email){
        this.firstName = first;
        this.lastName = last;
        this.email = _email;
    }
}
